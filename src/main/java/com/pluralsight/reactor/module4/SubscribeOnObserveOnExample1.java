package com.pluralsight.reactor.module4;

import com.pluralsight.reactor.utility.GateBasedSynchronization;
import com.pluralsight.reactor.utility.datasets.FibonacciSequence;
import com.pluralsight.reactor.utility.subscriber.DemoSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class SubscribeOnObserveOnExample1 {
    private final static Logger log = LoggerFactory.getLogger(SubscribeOnObserveOnExample1.class);

    public static void main(String[] args) {
        GateBasedSynchronization gate = new GateBasedSynchronization();

        // Our base observable for this example will be a FibonacciSequence with 10 numbers.
        Flux<Long> fibonacciFlux = FibonacciSequence.create(10)
                .doOnSubscribe(subscription -> {
                    log.info("fibonacciObservable::onSubscribe");
                });

        // -----------------------------------------------------------------------------------------

        // First, let's look at subscription with no threading modification.
        fibonacciFlux.subscribe(new DemoSubscriber());

        // No threading, but do our synchronization pattern anyway.
        gate.waitForAny("onError", "onComplete");
        log.info("--------------------------------------------------------");

        // -----------------------------------------------------------------------------------------
        // Scan the numbers on the computation thread pool
        // -----------------------------------------------------------------------------------------

        gate.resetAll();

        // SubscribeOn example illustrating how first SubscribeOn wins.
        fibonacciFlux
                .subscribeOn(Schedulers.boundedElastic())
                .subscribeOn(Schedulers.immediate())
                .subscribe(new DemoSubscriber<>());

        // No threading, but do our synchronization pattern anyway.
        gate.waitForAny("onError", "onComplete");
        log.info("--------------------------------------------------------");

        // -----------------------------------------------------------------------------------------
        // Illustrate how observeOn's position effects which scheduler is used.
        // -----------------------------------------------------------------------------------------

        gate.resetAll();

        // Illustrate how observeOn's position alters the scheduler that is
        // used for the observation portion of the code.
        fibonacciFlux
                // First observeOn...will be altered by the
                // observeOn further downstream.
                .publishOn(Schedulers.boundedElastic())
                // The location of subscribeOn doesn't matter.
                // First subscribeOn always wins.
                .subscribeOn(Schedulers.newSingle("Test"))

                // the last observeOn takes precedence.
                .publishOn(Schedulers.immediate())

                .subscribe(new DemoSubscriber<>());

        // No threading, but do our synchronization pattern anyway.
        gate.waitForAny("onError", "onComplete");
        log.info("--------------------------------------------------------");

        System.exit(0);
    }
}

package com.pluralsight.reactor.module4;

import com.pluralsight.reactor.nitrite.entity.LetterPair;
import com.pluralsight.reactor.utility.datasets.GreekAlphabet;
import com.pluralsight.reactor.utility.subscriber.DemoSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

public class ZipExample1 {
    private final Logger log = LoggerFactory.getLogger(ZipExample1.class);

    public static void main(String[] args) {

        // The zip operator is used to weave together up to 9 streams into
        // a stream of single events.
        Flux<LetterPair> greekWithEnglishObservable = Flux
                .zip(
                        GreekAlphabet.greekAlphabetInGreekFlux(),
                        GreekAlphabet.greekAlphabetInEnglishFlux(),
                        // ...for each observable entry, we return a LetterPair "zipping" them
                        // together.
                        (greekLetter, english) -> new LetterPair(greekLetter, english)
                );

        greekWithEnglishObservable.subscribe(new DemoSubscriber<>());

        System.exit(0);
    }
}

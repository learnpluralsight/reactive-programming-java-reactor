package com.pluralsight.reactor.module4;

import com.pluralsight.reactor.utility.datasets.GreekAlphabet;
import com.pluralsight.reactor.utility.subscriber.DemoSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

public class MapExample1 {
    private final static Logger log = LoggerFactory.getLogger(MapExample1.class);

    public static void main(String[] args) {

        // Take each english representation and return a string that
        // contains the string length of each one...
        Flux<Integer> lengthStream =
                GreekAlphabet.greekAlphabetInEnglishFlux()
                        .map(nextRepresentation -> nextRepresentation.length() );

        lengthStream.subscribe(new DemoSubscriber<>());

        System.exit(0);
    }
}

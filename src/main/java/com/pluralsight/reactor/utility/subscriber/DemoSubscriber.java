package com.pluralsight.reactor.utility.subscriber;

import com.pluralsight.reactor.utility.GateBasedSynchronization;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class DemoSubscriber<TEvent> implements Subscriber<TEvent> {

    private static final Logger log = LoggerFactory.getLogger(DemoSubscriber.class);

    private final GateBasedSynchronization gate;
    private Subscription subscription;
    private final String errorGateName;
    private final String successGateName;
    private final String completeGateName;

    private final long onNextDelayDuration;
    private final TimeUnit onNextDelayTimeUnit;

    public DemoSubscriber() {
        this.gate = new GateBasedSynchronization();
        this.errorGateName = "onError";
        this.successGateName = "onSuccess";
        this.completeGateName = "onComplete";

        this.onNextDelayDuration = Long.MIN_VALUE;
        this.onNextDelayTimeUnit = null;
    }

    public DemoSubscriber(GateBasedSynchronization gate) {
        this.gate = gate;
        this.errorGateName = "onError";
        this.successGateName = "onSuccess";
        this.completeGateName = "onComplete";

        this.onNextDelayDuration = 0L;
        this.onNextDelayTimeUnit = TimeUnit.SECONDS;
    }


    public DemoSubscriber(long l, TimeUnit milliseconds, GateBasedSynchronization gate, String onError, String onComplete){
        this.gate = new GateBasedSynchronization();
        this.errorGateName = "onError";
        this.successGateName = "onSuccess";
        this.completeGateName = "onComplete";

        this.onNextDelayDuration = Long.MIN_VALUE;
        this.onNextDelayTimeUnit = null;
    }

    public DemoSubscriber(GateBasedSynchronization gate, String onError, String onSuccess) {
        this.gate = new GateBasedSynchronization();
        this.errorGateName = "onError";
        this.successGateName = "onSuccess";
        this.completeGateName = null;

        this.onNextDelayDuration = Long.MIN_VALUE;
        this.onNextDelayTimeUnit = null;
    }

    public DemoSubscriber(GateBasedSynchronization gate, String onError, String onSuccess, String onComplete) {
        this.gate = new GateBasedSynchronization();
        this.errorGateName = "onError";
        this.successGateName = "onSuccess";
        this.completeGateName = "onComplete";

        this.onNextDelayDuration = Long.MIN_VALUE;
        this.onNextDelayTimeUnit = null;
    }

    public DemoSubscriber(GateBasedSynchronization gate, String successGateName) {
        this.gate = gate;
        this.successGateName = successGateName;
        this.errorGateName = "onError";
        this.completeGateName = "onComplete";

        this.onNextDelayDuration = 0L;
        this.onNextDelayTimeUnit = TimeUnit.SECONDS;
    }

    @Override
    public void onSubscribe(Subscription subscription) {
        this.subscription = subscription;
        subscription.request(Long.MAX_VALUE);
        log.info( "onSubscribe" );
    }

    public void onCancel() {
        this.subscription.cancel();
        log.info( "onCancel" );
    }

    @Override
    public void onNext(TEvent tEvent) {
        log.info( "onSuccess - {}" , tEvent);
        gate.openGate(successGateName);
    }

    @Override
    public void onError(Throwable e) {
        log.error( "onError - {}" , e.getMessage());
        log.error(e.getMessage(),e);
        gate.openGate(errorGateName);
    }

    @Override
    public void onComplete() {
        log.info( "onComplete" );
        gate.openGate(completeGateName);
    }


}

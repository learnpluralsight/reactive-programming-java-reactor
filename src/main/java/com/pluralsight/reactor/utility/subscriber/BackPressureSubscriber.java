package com.pluralsight.reactor.utility.subscriber;

import com.pluralsight.reactor.utility.GateBasedSynchronization;
import com.pluralsight.reactor.utility.ThreadHelper;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class BackPressureSubscriber <TEvent> implements Subscriber<TEvent> {
    private static final Logger log = LoggerFactory.getLogger(DemoSubscriber.class);

    private final GateBasedSynchronization gate;
    private final String errorGateName;
    private final String completeGateName;

    private final long onNextDelayDuration;
    private final TimeUnit onNextDelayTimeUnit;

    public BackPressureSubscriber() {
        this.gate = new GateBasedSynchronization();
        this.errorGateName = "onError";
        this.completeGateName = "onComplete";
        this.onNextDelayDuration = 0L;
        this.onNextDelayTimeUnit = TimeUnit.SECONDS;
    }

    public BackPressureSubscriber(GateBasedSynchronization gate) {
        this.gate = gate;
        this.errorGateName = "onError";
        this.completeGateName = "onComplete";

        this.onNextDelayDuration = 0L;
        this.onNextDelayTimeUnit = TimeUnit.SECONDS;
    }

    public BackPressureSubscriber(GateBasedSynchronization gate, String errorGateName, String completeGateName) {
        this.gate = gate;
        this.errorGateName = errorGateName;
        this.completeGateName = completeGateName;

        this.onNextDelayDuration = 0L;
        this.onNextDelayTimeUnit = TimeUnit.SECONDS;
    }

    public BackPressureSubscriber(long onNextDelayDuration , TimeUnit onNextDelayTimeUnit, GateBasedSynchronization gate,
                          String errorGateName, String completeGateName) {
        this.gate = gate;
        this.errorGateName = errorGateName;
        this.completeGateName = completeGateName;

        this.onNextDelayDuration = onNextDelayDuration;
        this.onNextDelayTimeUnit = onNextDelayTimeUnit;
    }

    public GateBasedSynchronization getGates() {
        return this.gate;
    }

    @Override
    public void onSubscribe(Subscription subscription) {
        subscription.request(Long.MAX_VALUE);
        log.info("onSubscribe");
    }

    @Override
    public void onNext(TEvent tEvent) {
        log.info( "onNext - {}" , tEvent == null ? "<NULL>" : tEvent.toString());

        // Drag our feet if requested to do so...
        if( onNextDelayDuration > 0 ) {
            ThreadHelper.sleep(onNextDelayDuration, onNextDelayTimeUnit);
        }
    }

    @Override
    public void onError(Throwable e) {
        log.error( "onError - {}" , e.getMessage());
        gate.openGate(errorGateName);
    }

    @Override
    public void onComplete() {
        log.info( "onComplete" );
        gate.openGate(completeGateName);
    }
}

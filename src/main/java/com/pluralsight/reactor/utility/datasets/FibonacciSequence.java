package com.pluralsight.reactor.utility.datasets;

import reactor.core.publisher.Flux;

import java.util.ArrayList;

public class FibonacciSequence {

    public static Flux<Long> create(final long totalNumbers) {

        return Flux.create(emitter -> {

            long count = 0;
            long previousValue1 = 1;
            long previousValue2 = 1;

            while( count < totalNumbers ) {

                if( emitter.isCancelled() ) {
                    break;
                }

                ++count;

                if( count == 1 ) {
                    emitter.next(0L);
                    continue;
                }

                if( count == 2 ) {
                    emitter.next(1L);
                    continue;
                }

                long newValue = previousValue1 + previousValue2;
                emitter.next(newValue);

                previousValue1 = previousValue2;
                previousValue2 = newValue;
            }

            if( !emitter.isCancelled() ) {
                emitter.complete();
            }
        });

    }

    public static Long[] toArray( int totalNumbers ) {

        return create(totalNumbers)
                .collectList()
                .block()
                .toArray(new Long[totalNumbers]);

//        return create(totalNumbers)
//                .collectInto(new ArrayList<Long>(totalNumbers), (collectionTarget, nextValue) -> collectionTarget.add(nextValue))
//                .blockingGet()
//                .toArray(new Long[totalNumbers]);

    }

    public static ArrayList<Long> toArrayList( int totalNumbers ) {
        return (ArrayList<Long>) create(totalNumbers)
                .collectList()
                .block();

//        return create(totalNumbers)
//                .collectInto(new ArrayList<Long>(totalNumbers), (collectionTarget, nextValue) -> collectionTarget.add(nextValue))
//                .blockingGet();
    }

}

package com.pluralsight.reactor.module6;

import com.pluralsight.reactor.utility.GateBasedSynchronization;
import com.pluralsight.reactor.utility.network.HttpResponseFluxFactory;
import com.pluralsight.reactor.utility.subscriber.DemoSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.net.URI;
import java.time.Duration;

public class NetworkingExample2 {

    private static final Logger log = LoggerFactory.getLogger(NetworkingExample2.class);

    public static void main(String[] args) {

        try {
            // Synchronization gates
            GateBasedSynchronization gate = new GateBasedSynchronization();

            // Create two requests to our addition service.  The first will have a long delay
            URI request1 = new URI("http://localhost:22221/addition?a=5&b=9&delay=6000");

            // The second request will no have a delay.
            URI request2 = new URI("http://localhost:22221/addition?a=21&b=21&delay=0");

            // Use our HttpResponseObserverFactory to makeObservable an Observable that returns
            // the result of the call to the addition service for the first request.
            // Note that we are placing this request on the IO thread pool since it
            // will be waiting on IO predominantly.
            Mono<Integer> networkRequest1 =
                    HttpResponseFluxFactory.additionRequestResponseMono(request1)
                            .subscribeOn(Schedulers.boundedElastic());

            // ...and another for the second request.
            Mono<Integer> networkRequest2 =
                    HttpResponseFluxFactory.additionRequestResponseMono(request2)
                            .subscribeOn(Schedulers.boundedElastic());

            // We use the merge operator with maxConcurrency of 2 in order
            // to cause both networkRequest1 and networkRequest2 to be executed
            // simultaneously.  We want all of this on the IO threads.
            // We also set a timeout of 5 seconds for the requests.
            Flux<Integer> responseStream = Flux.merge(
                    networkRequest1, networkRequest2)
                    .subscribeOn(Schedulers.boundedElastic())
                    .timeout(Duration.ofSeconds(5L) , Flux.just(-1));

            // No that we have our Observable chain, we can use our standard
            // DemoSubscriber to cause it to execute.
            responseStream.subscribe(new DemoSubscriber<>(gate));

            // We wait for success or failure.
            gate.waitForAny("onError", "onComplete");

        } catch (Throwable e) {
            log.error(e.getMessage(),e);
        }

    }
}

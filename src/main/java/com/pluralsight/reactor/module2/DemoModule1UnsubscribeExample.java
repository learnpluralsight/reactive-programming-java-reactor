package com.pluralsight.reactor.module2;

import com.pluralsight.reactor.utility.GateBasedSynchronization;
import com.pluralsight.reactor.utility.datasets.GreekAlphabet;
import com.pluralsight.reactor.utility.datasets.GreekLetterPair;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

import java.util.concurrent.atomic.AtomicInteger;

public class DemoModule1UnsubscribeExample {
    private static Logger log = LoggerFactory.getLogger(DemoModule1UnsubscribeExample.class);

    public static void main(String[] args) {

        // Simple counter so we can cut things off in the middle...
        AtomicInteger counter = new AtomicInteger(0);

        // Synchronization magic.
        GateBasedSynchronization gate = new GateBasedSynchronization();

        Flux
                .fromArray(GreekAlphabet.greekLetters)
                .subscribe(new Subscriber<String>() {
                    private Subscription subscription;
                    @Override
                    public void onSubscribe(Subscription subscription) {
                        subscription.request(Long.MAX_VALUE);
                        this.subscription = subscription;
                    }

                    @Override
                    public void onNext(String nextLetter) {
                        log.info( "onNext - {}" , nextLetter );

                        // Once we have reached 5 events, we want to unsubscribe
                        if( counter.incrementAndGet() >= 5) {

                            // We hit how many messages we want, so we call
                            // dispose on our subscription.  This will make the Observable
                            // stop sending us onNext events.
                            subscription.cancel();

                            // We open the "eventMaxReached" gate so that it will allow
                            // the main thread to terminate.
                            gate.openGate("eventMaxReached");
                        }
                    }

                    // onError is called when any exception is thrown either
                    // in the Observable code, or from the Observer code.
                    @Override
                    public void onError(Throwable e) {

                        // Send the error message to the log.
                        log.error("onError - {}" , e.getMessage());

                        // Open the gate for "onError" so that the main
                        // thread will be allowed to continue.
                        gate.openGate("onError");
                    }

                    // onComplete is called when the Observable finishes emitting
                    // all events.  If onError is called, you will not see an
                    // onComplete call.  Likewise, once onComplete is called, onError
                    // is guaranteed not to be called.
                    @Override
                    public void onComplete() {
                        log.info( "onComplete" );
                        gate.openGate("onComplete");
                    }
                });

        // Wait for one of these gates to open: "eventMaxReached", "onComplete" or "onError"
        gate.waitForAny("eventMaxReached", "onComplete", "onError");

        // Is the "eventMaxReached" gate open?
        log.info("eventMaxReached gate status: {}" , gate.isGateOpen("eventMaxReached"));

        // Did "onComplete" get called?
        log.info("onComplete      gate status: {}" , gate.isGateOpen("onComplete"));

        // Did "onError" get called?
        log.info("onError         gate status: {}" , gate.isGateOpen("onError"));

        System.exit(0);
    }
}

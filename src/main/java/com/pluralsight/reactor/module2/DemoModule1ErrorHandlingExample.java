package com.pluralsight.reactor.module2;

import com.pluralsight.reactor.utility.GateBasedSynchronization;
import com.pluralsight.reactor.utility.datasets.GreekAlphabet;
import com.pluralsight.reactor.utility.datasets.GreekLetterPair;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

import java.util.concurrent.atomic.AtomicInteger;

public class DemoModule1ErrorHandlingExample {

    private static Logger log = LoggerFactory.getLogger(DemoModule1ErrorHandlingExample.class);

    public static void main(String[] args) {

        // My synchronization magic.  Let's keep this thread from exiting
        // until all of our test code has executed.
        GateBasedSynchronization gate = new GateBasedSynchronization();

        AtomicInteger counter = new AtomicInteger();

        Flux<GreekLetterPair> zipTogether = Flux.zip(
                GreekAlphabet.greekAlphabetInGreekFlux(),
                GreekAlphabet.greekAlphabetInEnglishFlux(),
                (greekLetter, englishLetter) -> {
                    if (counter.incrementAndGet() == 5){
                        throw  new IllegalStateException("Boom!");
                    } else {
                        return new GreekLetterPair(greekLetter, englishLetter);
                    }
                }
        );

        zipTogether.onErrorReturn(new GreekLetterPair("kepaia", "BOOM"))
                .subscribe(new Subscriber<GreekLetterPair>() {
                    @Override
                    public void onSubscribe(Subscription subscription) {
                        subscription.request(Long.MAX_VALUE);
                        log.info("onSubscribe");
                    }

                    @Override
                    public void onNext(GreekLetterPair greekLetterPair) {
                        log.info("onNext - ({}, {})", greekLetterPair.getGreekLetter(), greekLetterPair.getEnglishLetter());
                    }

                    @Override
                    public void onError(Throwable e) {
                        log.info("onError - {}", e.getMessage());
                        gate.openGate("onError");
                    }

                    @Override
                    public void onComplete() {
                        log.info("onComplete");
                        gate.openGate("onComplete");
                    }
                });
        }
}

package com.pluralsight.reactor.module2;

import com.pluralsight.reactor.utility.GateBasedSynchronization;
import com.pluralsight.reactor.utility.datasets.GreekAlphabet;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

public class DemoModule1EventSequenceExample {
    private static Logger log = LoggerFactory.getLogger(DemoModule1EventSequenceExample.class);

    public static void main(String[] args) {
        // My synchronization magic.  Let's keep this thread from exiting
        // until all of our test code has executed.
        GateBasedSynchronization gate = new GateBasedSynchronization();

        // Create an Observable<String> that contains the 24 greek letters.
        Flux
                .fromArray(GreekAlphabet.greekLetters)
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onSubscribe(Subscription subscription) {
                        subscription.request(Long.MAX_VALUE);
                        log.info( "onSubscribe" );
                    }

                    public void onNext(String nextLetter) {
                        log.info( "onNext - {}" , nextLetter );
                    }

                    public void onError(Throwable throwable) {
                        // Send the error message to the log.
                        log.error("onError - {}" , throwable.getMessage());
                    }

                    public void onComplete() {
                        log.info( "onComplete" );
                    }
                });

        // Wait for either "onComplete" or "onError" to be called.
        gate.waitForAny("onComplete", "onError");

        System.exit(0);
    }
}

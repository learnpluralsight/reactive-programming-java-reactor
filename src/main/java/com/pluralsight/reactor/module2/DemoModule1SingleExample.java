package com.pluralsight.reactor.module2;

import com.pluralsight.reactor.utility.GateBasedSynchronization;
import com.pluralsight.reactor.utility.datasets.GreekAlphabet;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class DemoModule1SingleExample {
    private static Logger log = LoggerFactory.getLogger(DemoModule1SingleExample.class);

    public static void main(String[] args) {

        // My synchronization magic.  Let's keep this thread from exiting
        // until all of our test code has executed.
        GateBasedSynchronization gate = new GateBasedSynchronization();

        Mono<String> targetMono = Flux
                .fromArray(GreekAlphabet.greekLetters)
                .next()
                .defaultIfEmpty("A");

        targetMono.subscribe(new MySubscriber());

        // Wait for either "onSuccess" or "onError" to be called.
        gate.waitForAny("onComplete", "onError");

        System.exit(0);
    }

    public static class MySubscriber implements Subscriber<String>{

        public void onSubscribe(Subscription subscription) {
            subscription.request(Long.MAX_VALUE);
            log.info( "onSubscribe" );
        }

        public void onNext(String nextLetter) {
            log.info( "onNext - {}" , nextLetter );
        }

        public void onError(Throwable throwable) {
            // Send the error message to the log.
            log.error("onError - {}" , throwable.getMessage());
        }

        public void onComplete() {
            log.info( "onComplete" );
        }
    }
}

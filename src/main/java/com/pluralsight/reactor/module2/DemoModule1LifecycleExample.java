package com.pluralsight.reactor.module2;

import com.pluralsight.reactor.utility.GateBasedSynchronization;
import com.pluralsight.reactor.utility.datasets.GreekAlphabet;
import com.pluralsight.reactor.utility.datasets.GreekLetterPair;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

public class DemoModule1LifecycleExample {
    private static Logger log = LoggerFactory.getLogger(DemoModule1LifecycleExample.class);

    public static void main(String[] args) {
        // Synchronization magic.
        GateBasedSynchronization gate = new GateBasedSynchronization();

        // Create an Observable and store it to a local variable.
        // This will "zip" together two streams of the same length into a single
        // stream of a composite object (GreekLetterPair).
        //( greekLetter, englishLetter) -> new GreekLetterPair(greekLetter, englishLetter)
        Flux<GreekLetterPair> zipTogether = Flux.zip(
                GreekAlphabet.greekAlphabetInGreekFlux(),
                GreekAlphabet.greekAlphabetInEnglishFlux(),
                ( greekLetter, englishLetter) -> new GreekLetterPair(greekLetter, englishLetter)
        );

        // Subscribe to the zip observable and have it generate it's output.
        subscribeToZipObservable(gate, zipTogether);

        // Wait for either "onComplete" or "onError" to be called.
        gate.waitForAny("onComplete", "onError");

        // Reset all synchronization gates.
        gate.resetAll();

        log.info("--------------------------------------------------------------------------------");

        // Subscribe to the zip observable and have it generate it's output.
        subscribeToZipObservable(gate, zipTogether);

        // Wait for either "onComplete" or "onError" to be called.
        gate.waitForAny("onComplete", "onError");

        // Reset all synchronization gates.
        gate.resetAll();

        System.exit(0);
    }

    private static void subscribeToZipObservable(GateBasedSynchronization gate, Flux<GreekLetterPair> zipTogether) {

        zipTogether.subscribe(new Subscriber<GreekLetterPair>() {
            @Override
            public void onSubscribe(Subscription subscription) {
                subscription.request(Long.MAX_VALUE);
                log.info("onSubscribe");
            }


            @Override
            public void onNext(GreekLetterPair greekLetterPair) {
                log.info("onNext - ({}, {})", greekLetterPair.getGreekLetter(), greekLetterPair.getEnglishLetter());
            }

            @Override
            public void onError(Throwable e) {
                log.info("onError - {}", e.getMessage());
                gate.openGate("onError");
            }

            @Override
            public void onComplete() {
                log.info("onComplete");
                gate.openGate("onComplete");
            }
        });
    }
}

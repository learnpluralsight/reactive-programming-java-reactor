package com.pluralsight.reactor.module2;

import com.pluralsight.reactor.utility.GateBasedSynchronization;
import com.pluralsight.reactor.utility.subscriber.BackPressureSubscriber;
import com.pluralsight.reactor.utility.subscriber.DemoSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.util.concurrent.TimeUnit;

public class DemoModule1BackpressureExample {
    private static Logger log = LoggerFactory.getLogger(DemoModule1BackpressureExample.class);

    public static void main(String[] args) {

        // Synchronization helper
        GateBasedSynchronization gate = new GateBasedSynchronization();

        Flux<Integer> rangeOfNumbers = Flux.range(1, 1000000)
                .repeat()
                .doOnNext(nexInt->log.info("emitting int {}", nexInt))
                .subscribeOn(Schedulers.single());

        // Create a DemoSubscriber with a slight delay of 10ms.
        // This should make the rangeOfNumber's emission far outpace
        // the subscriber.
        BackPressureSubscriber<Integer> demoSubscriber = new BackPressureSubscriber<Integer>(
                10L, TimeUnit.MILLISECONDS,
                gate, "onError", "onComplete"
        );

        // Subscribe to start the numbers flowing.
        rangeOfNumbers.subscribe(demoSubscriber);

        // Wait for things to finish
        gate.waitForAny("onError", "onComplete");

        System.exit(0);
    }
}

package com.pluralsight.reactor.module3;

import com.pluralsight.reactor.utility.ThreadHelper;
import com.pluralsight.reactor.utility.subscriber.DemoSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.Observable;
import java.util.concurrent.TimeUnit;

public class SampleExample1 {

    private final static Logger log = LoggerFactory.getLogger(SampleExample1.class);

    public static void main(String[] args) {

        // Create a repeating greek alphabet
        Flux<Long> incrementingObservable = Flux.interval(Duration.ofMillis(0l), Duration.ofMillis(50))
                // Like timeout, sample must use a different thread pool
                // so that it can send a message event though events
                // may be being generated on the main thread.
                .subscribeOn(Schedulers.single())
                // Sample the stream every 2 seconds.
                .sample(Duration.ofMillis(100l));


        // Subscribe and watch the emit happen every 2 seconds.
        incrementingObservable.subscribe(new DemoSubscriber<>());

        // Wait for 10 seconds
        ThreadHelper.sleep(10, TimeUnit.SECONDS);

        System.exit(0);
    }

}

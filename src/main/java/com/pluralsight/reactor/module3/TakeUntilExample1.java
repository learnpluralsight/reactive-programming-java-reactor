package com.pluralsight.reactor.module3;

import com.pluralsight.reactor.utility.datasets.GreekAlphabet;
import com.pluralsight.reactor.utility.subscriber.DemoSubscriber;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class TakeUntilExample1 {
    public static void main(String[] args) {

        // Get the usual Greek alphabet and repeat it FOREVER!
        Flux<String> greekAlphabet = GreekAlphabet.greekAlphabetInEnglishFlux()
                .repeat();

        // We want to take for 2 seconds.
        greekAlphabet
                .takeUntilOther(Flux.interval(Duration.ofSeconds(2), Duration.ofSeconds(10)))
                .subscribe(new DemoSubscriber<>());

        System.exit(0);
    }
}

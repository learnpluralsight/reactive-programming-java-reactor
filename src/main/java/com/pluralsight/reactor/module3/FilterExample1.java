package com.pluralsight.reactor.module3;

import com.pluralsight.reactor.utility.datasets.GreekAlphabet;
import com.pluralsight.reactor.utility.subscriber.DemoSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

public class FilterExample1 {
    private static final Logger log = LoggerFactory.getLogger(FilterExample1.class);

    public static void main(String[] args) {

        // Create an Observable to filter.
        Flux<String> greekAlphabet = GreekAlphabet.greekAlphabetInEnglishFlux()

                // Filter out "delta"
                .filter(nextLetter -> !nextLetter.equals("delta"));

        greekAlphabet.subscribe(new DemoSubscriber<>());

        System.exit(0);
    }
}

package com.pluralsight.reactor.module3;

import com.pluralsight.reactor.utility.datasets.FibonacciSequence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

import java.util.Observable;

public class PositionalExample1 {
    private final static Logger log = LoggerFactory.getLogger(PositionalExample1.class);

    public static void main(String[] args) {

        // Create a Fibonacci sequence
        Flux<Long> numberSequence = FibonacciSequence.create(10);

        // Demonstrate the "first" operator be emitting on the first
        // number in the sequence which should be zero.
        log.info("first Example");
        numberSequence
                .blockFirst()
                .toString();

        log.info("");
        log.info( "----------------------------------------------------------------------------");
        log.info("");

        // Demonstrate the "first" operator on an Observable that has no items.
        // The default value should be emitted.
        log.info("first with default Example");
        Flux
                .empty()
                .switchIfEmpty(Flux.just(1l))
                .blockFirst()
                .toString();


        log.info("");
        log.info( "----------------------------------------------------------------------------");
        log.info("");

        // Demonstrate the "firstOrError" operator on an Observable that has no items.
        // An error should be emitted.
//        log.info("firstOrError Example");
//        Observable.empty()
//                .firstOrError()
//                .subscribe(new SingleDemoSubscriber<>());

        log.info("");
        log.info( "----------------------------------------------------------------------------");
        log.info("");

        // Demonstrate the "firstElement" operator.
        // Zero should be emitted
        log.info("firstElement Example");
        numberSequence
                .blockFirst()
                .toString();

        log.info("");
        log.info( "----------------------------------------------------------------------------");
        log.info("");

        // Demonstrate the "firstElement" operator with an empty stream.
        // Only an onComplete should be emitted.
//        log.info("firstElement with empty stream Example");
//        Observable.empty()
//                .firstElement()
//                .subscribe(new MaybeDemoSubscriber<>());

        log.info("");
        log.info( "----------------------------------------------------------------------------");
        log.info("");

    }
}

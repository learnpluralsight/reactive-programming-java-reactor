package com.pluralsight.reactor.module3;

import com.pluralsight.reactor.utility.subscriber.DemoSubscriber;
import reactor.core.publisher.Flux;

public class CreationJustExample1 {

    public static void main(String[] args) {

        // "just" allows for the creation of Observables from single
        // values.
        Flux justFlux = Flux.just(42);

        // Output the single value.
        justFlux.subscribe(new DemoSubscriber());

        System.exit(0);
    }

}

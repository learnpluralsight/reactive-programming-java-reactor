package com.pluralsight.reactor.module3;

import com.pluralsight.reactor.utility.GateBasedSynchronization;
import com.pluralsight.reactor.utility.ThreadHelper;
import com.pluralsight.reactor.utility.datasets.GreekAlphabet;
import com.pluralsight.reactor.utility.subscriber.DemoSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class TimeoutExample {
    private final static Logger log = LoggerFactory.getLogger(TimeoutExample.class);

    public static void main(String[] args) {

        GateBasedSynchronization gate = new GateBasedSynchronization();

        // Create a custom Observable that will emit alpha, beta, pause for a day, and
        // then emit gamma.
        Flux<Object> greekAlphabetWithBigDelay = Flux.create(objectFluxSink -> {
            objectFluxSink.next(GreekAlphabet.greekLettersInEnglish[0]);
            objectFluxSink.next(GreekAlphabet.greekLettersInEnglish[1]);
            ThreadHelper.sleep(1, TimeUnit.DAYS);            // wait 1 days
            objectFluxSink.next(GreekAlphabet.greekLettersInEnglish[2]);  // Emit gamma
            objectFluxSink.complete();})
                // timeout emits the "timeout" onError on the computation thread pool
                // so we make the entire subscription happen on the same thread pool.
                // This is because the main thread may be the thread that is
                // hung and causes the timeout.
                .subscribeOn(Schedulers.single(),true)
                .timeout(Duration.ofSeconds(2l));

        greekAlphabetWithBigDelay.subscribe(new DemoSubscriber());

        gate.waitForAny("onComplete", "onError");

        System.exit(0);
    }
}

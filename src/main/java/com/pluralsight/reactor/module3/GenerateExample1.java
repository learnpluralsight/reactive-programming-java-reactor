package com.pluralsight.reactor.module3;

import com.pluralsight.reactor.utility.subscriber.DemoSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

public class GenerateExample1 {

    private final static Logger log = LoggerFactory.getLogger(GenerateExample1.class);

    public static void main(String[] args) {
        Flux<Integer> geometricSequence = makeFlux(1,2,8);
        geometricSequence.subscribe(new DemoSubscriber<>());
    }

    public static Flux<Integer> makeFlux(int start , int multiplier, int totalNumbers ) {
        return Flux.generate(() -> new GeometricSequenceState(start,multiplier,totalNumbers),
                (state, emitter) -> {
                    // Increment the number of values we have emitted
                    state.incrementCount();

                    // Emit the currently calculated
                    // value of the geometric sequence.
                    emitter.next(state.getCurrentValue());

                    // If we have reached the end, then emit and onComplete.
                    if(state.getCount() >= state.getTotalNumbers()) {
                        emitter.complete();
                    }

                    // Calculate the next value in the sequence.
                    state.generateNextValue();

                    return state;
                });
    }

    public static class GeometricSequenceState {

        private final int multiplier;
        private final int totalNumbers;

        private int count;
        private int currentValue;

        public GeometricSequenceState(int start, int multiplier, int totalNumbers) {
            this.multiplier = multiplier;
            this.totalNumbers = totalNumbers;

            this.count = 0;
            this.currentValue = start;
        }

        public int getTotalNumbers() {
            return totalNumbers;
        }

        public int getCount() {
            return count;
        }

        public int getCurrentValue() {
            return currentValue;
        }

        public void incrementCount() {
            ++this.count;
        }

        public void generateNextValue() {
            this.currentValue = this.currentValue * this.multiplier;
        }
    }
}

package com.pluralsight.reactor.module3;

import com.pluralsight.reactor.utility.GateBasedSynchronization;
import com.pluralsight.reactor.utility.ThreadHelper;
import com.pluralsight.reactor.utility.datasets.GreekAlphabet;
import com.pluralsight.reactor.utility.subscriber.DemoSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.Arrays;
import java.util.Observable;
import java.util.concurrent.TimeUnit;

public class TimeoutExample2 {
    private final static Logger log = LoggerFactory.getLogger(TimeoutExample2.class);

    public static void main(String[] args) {

        GateBasedSynchronization gate = new GateBasedSynchronization();


        // Create a custom Observable that will emit alpha, beta, pause for a day, and
        // then emit gamma.
        Flux<Object> greekAlphabetWithBigDelay = Flux.create(objectFluxSink -> {
            ThreadHelper.sleep(1, TimeUnit.DAYS);            // wait 1 days
            Arrays.stream(GreekAlphabet.greekLettersInEnglish)
                    .forEach(nextLetter-> objectFluxSink.next(nextLetter));

        })
                // timeout emits the "timeout" onError on the computation thread pool
                // so we make the entire subscription happen on the same thread pool.
                // This is because the main thread may be the thread that is
                // hung and causes the timeout.
                .subscribeOn(Schedulers.boundedElastic())
                .timeout(Duration.ofSeconds(2l), GreekAlphabet.greekAlphabetInGreekFlux());

        greekAlphabetWithBigDelay.subscribe(new DemoSubscriber());

        gate.waitForAny("onComplete", "onError");

        System.exit(0);
    }
}

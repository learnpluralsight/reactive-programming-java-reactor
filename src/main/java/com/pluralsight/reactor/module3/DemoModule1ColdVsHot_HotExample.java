package com.pluralsight.reactor.module3;

import com.pluralsight.reactor.utility.GateBasedSynchronization;
import com.pluralsight.reactor.utility.datasets.GreekAlphabet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

public class DemoModule1ColdVsHot_HotExample {
    private static Logger log = LoggerFactory.getLogger(DemoModule1ColdVsHot_HotExample.class);

    public static void main(String[] args) {

        // Synchronization magic.
        GateBasedSynchronization gate = new GateBasedSynchronization();

//        // Create a "hot" observable that emits greek letters at a furious pace.
//        // We only take the first 49 events to keep things understandable.
//        Flux<String> hotGreekAlphabet =
//                GreekAlphabet.greekAlphabetInEnglishHotObservable(true)
//                        .take(49);

    }
}
